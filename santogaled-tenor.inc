\context Staff = "tenor" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Tenor"
	\set Staff.shortInstrumentName = "T."
	\set Staff.midiInstrument = "Voice Oohs"
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-tenor" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\time 4/4
		\clef "treble_8"
		\key d \major

		R1*2  |
		d 4. d 8 ~ d 2  |
		e 4. e 4 ( d 8 cis 4 )  |
%% 5
		cis 4 d 2 fis 4  |
		e 4 d 8 cis ~ cis 2  |
		r4 b, 8 e 4 d 8 cis 4  |
		cis 1  |
		d 1  |
%% 10
		R1  |
		d 4. d 8 ~ d 2  |
		a 4. a 4 ( g 8 fis 4 )  |
		e 4 fis 2 b 4  |
		e 4 d 8 cis ~ cis 2  |
%% 15
		r4 e 8 g 4 fis 8 e 4  |
		e 1  |
		fis 1  |
		R1  |
		\time 2/2
		r4. g, 8 g g 4 fis 8 ~  |
%% 20
		fis 8 e 4 d d d 8  |
		cis 8 a, 4. r2  |
		r4. d 8 fis e d e  |
		d 1  |
		R1*2  |
		r4. d 8 fis e d e  |
		d 1 ~  |
		d 4 r8 fis fis fis 4 a 8 ~  |
		a 8 a 4 fis 4. ( g 8 fis  |
%% 30
		e 1 )  |
		\time 4/4
		d 4. d 8 ~ d 2  |
		e 4. e 4 ( d 8 cis 4 )  |
		cis 4 d 2 fis 4  |
		e 4 d 8 cis ~ cis 2  |
%% 35
		r4 b, 8 e 4 d 8 cis 4  |
		cis 1  |
		d 1  |
		R1  |
		d 4. d 8 ~ d 2  |
%% 40
		a 4. a 4 ( g 8 fis 4 )  |
		e 4 fis 2 b 4  |
		e 4 d 8 cis ~ cis 2  |
		r4 e 8 g 4 fis 8 e 4  |
		e 1  |
%% 45
		fis 1  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-tenor" {
		San -- to, __ san -- to, __ san -- to
		es el Se -- ñor, Dios del u -- ni -- ver -- so.
		San -- to, __ san -- to, __ san -- to
		es el Se -- ñor, Dios del u -- ni -- ver -- so.

		Lle -- nos es -- tán el cie -- "lo y" la tie -- rra
		¡Ho -- san -- "na en" el cie -- lo!
		¡Ho -- san -- "na en" el cie -- lo! __
		¡Ho -- san -- "na en" el cie -- lo! __

		San -- to, __ san -- to, __ san -- to
		es el Se -- ñor, Dios del u -- ni -- ver -- so.
		San -- to, __ san -- to, __ san -- to
		es el Se -- ñor, Dios del u -- ni -- ver -- so.
	}
>>
