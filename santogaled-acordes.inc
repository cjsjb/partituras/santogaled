\context ChordNames
	\chords {
	\set majorSevenSymbol = \markup { "maj7" }
	\set chordChanges = ##t

	% intro
	d1 d1

	% santo, santo, oh, santo...
	d1 a1 b1:m fis1:m
	g1 fis1 b1:m b1:m

	% santo, santo, oh, santo...
	d1 a1 b1:m fis1:m
	g1 fis1 b1:m b1:m

	% llenos estan...
	g1 d1 a1 b1:m
	g1 d1 a4. d4. b4:m b1:m
	g1 e1:m a1 a1

	% santo, santo, oh, santo...
	d1 a1 b1:m fis1:m
	g1 fis1 b1:m b1:m

	% santo, santo, oh, santo...
	d1
	r1*6
	}
