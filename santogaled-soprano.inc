\context Staff = "soprano" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Soprano"
	\set Staff.shortInstrumentName = "S."
	\set Staff.midiInstrument = "Voice Oohs"
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-soprano" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\time 4/4
		\clef "treble"
		\key d \major

		R1*2  |
		d' 4. d' 8 ~ d' 2  |
		e' 4. e' 4 ( d' 8 cis' 4 )  |
%% 5
		cis' 4 d' 2 fis' 4  |
		e' 4 d' 8 cis' ~ cis' 2  |
		r4 b 8 e' 4 d' 8 cis' 4  |
		cis' 1  |
		d' 1  |
%% 10
		R1  |
		d' 4. d' 8 ~ d' 2  |
		e' 4. e' 4 ( d' 8 cis' 4 )  |
		cis' 4 d' 2 fis' 4  |
		e' 4 d' 8 cis' ~ cis' 2  |
%% 15
		r4 b 8 e' 4 d' 8 cis' 4  |
		cis' 1  |
		d' 1  |
		R1  |
		\time 2/2
		r4. g 8 g' g' 4 fis' 8 ~  |
%% 20
		fis' 8 e' 4 d' d' d' 8  |
		cis' 8 a 4. r4 e' 8 d'  |
		cis' 8 d' 2. r8  |
		r4 r8 g g' g' g' fis' ~  |
		fis' 8 e' 4 d' d' 4.  |
%% 25
		cis' 4. e' d' 4 ~  |
		d' 1  |
		R1  |
		r4. fis' 8 fis' fis' 4 a' 8 ~  |
		a' 8 a' 4 fis' 4. ( g' 8 fis'  |
%% 30
		e' 1 )  |
		\time 4/4
		d' 4. d' 8 ~ d' 2  |
		e' 4. e' 4 ( d' 8 cis' 4 )  |
		cis' 4 d' 2 fis' 4  |
		e' 4 d' 8 cis' ~ cis' 2  |
%% 35
		r4 b 8 e' 4 d' 8 cis' 4  |
		cis' 1  |
		d' 1  |
		R1  |
		d' 4. d' 8 ~ d' 2  |
%% 40
		e' 4. e' 4 ( d' 8 cis' 4 )  |
		cis' 4 d' 2 fis' 4  |
		e' 4 d' 8 cis' ~ cis' 2  |
		r4 b 8 e' 4 d' 8 cis' 4  |
		cis' 1  |
%% 45
		d' 1  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-soprano" {
		San -- to, __ san -- to, __ san -- to
		es el Se -- ñor, Dios del u -- ni -- ver -- so.
		San -- to, __ san -- to, __ san -- to
		es el Se -- ñor, Dios del u -- ni -- ver -- so.

		Lle -- nos es -- tán el cie -- "lo y" la tie -- rra
		de tu glo -- ria.
		Ben -- di -- "to el" que vie -- "ne en" nom -- bre del Se -- ñor. __
		¡Ho -- san -- "na en" el cie -- lo! __

		San -- to, __ san -- to, __ san -- to
		es el Se -- ñor, Dios del u -- ni -- ver -- so.
		San -- to, __ san -- to, __ san -- to
		es el Se -- ñor, Dios del u -- ni -- ver -- so.
	}
>>
